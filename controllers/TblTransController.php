<?php

namespace app\controllers;

use Yii;
use app\models\TblTrans;
use app\models\TblTransSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TblTransController implements the CRUD actions for TblTrans model.
 */
class TblTransController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblTrans models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblTransSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblTrans model.
     * @param integer $JournalNumber
     * @param integer $SequenceNumber
     * @param integer $Year
     * @param string $AccountKey
     * @param string $AcctType
     * @return mixed
     */
    public function actionView($JournalNumber, $SequenceNumber, $Year, $AccountKey, $AcctType)
    {
        return $this->render('view', [
            'model' => $this->findModel($JournalNumber, $SequenceNumber, $Year, $AccountKey, $AcctType),
        ]);
    }

    /**
     * Creates a new TblTrans model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblTrans();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'JournalNumber' => $model->JournalNumber, 'SequenceNumber' => $model->SequenceNumber, 'Year' => $model->Year, 'AccountKey' => $model->AccountKey, 'AcctType' => $model->AcctType]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TblTrans model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $JournalNumber
     * @param integer $SequenceNumber
     * @param integer $Year
     * @param string $AccountKey
     * @param string $AcctType
     * @return mixed
     */
    public function actionUpdate($JournalNumber, $SequenceNumber, $Year, $AccountKey, $AcctType)
    {
        $model = $this->findModel($JournalNumber, $SequenceNumber, $Year, $AccountKey, $AcctType);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'JournalNumber' => $model->JournalNumber, 'SequenceNumber' => $model->SequenceNumber, 'Year' => $model->Year, 'AccountKey' => $model->AccountKey, 'AcctType' => $model->AcctType]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblTrans model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $JournalNumber
     * @param integer $SequenceNumber
     * @param integer $Year
     * @param string $AccountKey
     * @param string $AcctType
     * @return mixed
     */
    public function actionDelete($JournalNumber, $SequenceNumber, $Year, $AccountKey, $AcctType)
    {
        $this->findModel($JournalNumber, $SequenceNumber, $Year, $AccountKey, $AcctType)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblTrans model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $JournalNumber
     * @param integer $SequenceNumber
     * @param integer $Year
     * @param string $AccountKey
     * @param string $AcctType
     * @return TblTrans the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($JournalNumber, $SequenceNumber, $Year, $AccountKey, $AcctType)
    {
        if (($model = TblTrans::findOne(['JournalNumber' => $JournalNumber, 'SequenceNumber' => $SequenceNumber, 'Year' => $Year, 'AccountKey' => $AccountKey, 'AcctType' => $AcctType])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
