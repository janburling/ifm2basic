<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblLiquidationTrans */

$this->title = 'Create Tbl Liquidation Trans';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Liquidation Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-liquidation-trans-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
