<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TlkpAcctType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tlkp-acct-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'AcctType')->textInput(['maxlength' => 1]) ?>

    <?= $form->field($model, 'Description')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'PrintSeq')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
