<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TlkpAcctType */

$this->title = 'Create Tlkp Acct Type';
$this->params['breadcrumbs'][] = ['label' => 'Tlkp Acct Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tlkp-acct-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
