<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblEncTransSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-enc-trans-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'JournalNumber') ?>

    <?= $form->field($model, 'EncFund') ?>

    <?= $form->field($model, 'EncYear') ?>

    <?= $form->field($model, 'EncPONumber') ?>

    <?= $form->field($model, 'VendorId') ?>

    <?php // echo $form->field($model, 'Amount') ?>

    <?php // echo $form->field($model, 'Balance') ?>

    <?php // echo $form->field($model, 'EStatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
