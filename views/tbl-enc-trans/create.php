<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblEncTrans */

$this->title = 'Create Tbl Enc Trans';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Enc Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-enc-trans-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
