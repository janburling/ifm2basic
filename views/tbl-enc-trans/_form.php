<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblEncTrans */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-enc-trans-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'JournalNumber')->textInput() ?>

    <?= $form->field($model, 'EncFund')->textInput(['maxlength' => 3]) ?>

    <?= $form->field($model, 'EncYear')->textInput() ?>

    <?= $form->field($model, 'EncPONumber')->textInput() ?>

    <?= $form->field($model, 'VendorId')->textInput() ?>

    <?= $form->field($model, 'Amount')->textInput() ?>

    <?= $form->field($model, 'Balance')->textInput() ?>

    <?= $form->field($model, 'EStatus')->textInput(['maxlength' => 1]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
