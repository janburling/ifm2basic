<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblEncTrans */

$this->title = 'Update Tbl Enc Trans: ' . ' ' . $model->JournalNumber;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Enc Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->JournalNumber, 'url' => ['view', 'id' => $model->JournalNumber]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-enc-trans-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
