<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblEncTransSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Enc Trans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-enc-trans-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tbl Enc Trans', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'JournalNumber',
            'EncFund',
            'EncYear',
            'EncPONumber',
            'VendorId',
            // 'Amount',
            // 'Balance',
            // 'EStatus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
