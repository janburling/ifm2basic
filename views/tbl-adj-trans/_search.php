<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblAdjTransSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-adj-trans-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'JournalNumber') ?>

    <?= $form->field($model, 'DocYear') ?>

    <?= $form->field($model, 'DocNum') ?>

    <?= $form->field($model, 'ReverseEntry')->checkbox() ?>

    <?= $form->field($model, 'ReverseDate') ?>

    <?php // echo $form->field($model, 'ReverseJrnl') ?>

    <?php // echo $form->field($model, 'CashAdjType') ?>

    <?php // echo $form->field($model, 'DepositDate') ?>

    <?php // echo $form->field($model, 'ReceiptNbr') ?>

    <?php // echo $form->field($model, 'AdjType') ?>

    <?php // echo $form->field($model, 'Invoice') ?>

    <?php // echo $form->field($model, 'ClientId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
