<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblAdjTransSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Adj Trans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-adj-trans-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tbl Adj Trans', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'JournalNumber',
            'DocYear',
            'DocNum',
            'ReverseEntry:boolean',
            'ReverseDate',
            // 'ReverseJrnl',
            // 'CashAdjType',
            // 'DepositDate',
            // 'ReceiptNbr',
            // 'AdjType',
            // 'Invoice',
            // 'ClientId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
