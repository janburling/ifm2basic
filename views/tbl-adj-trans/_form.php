<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblAdjTrans */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-adj-trans-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'JournalNumber')->textInput() ?>

    <?= $form->field($model, 'DocYear')->textInput() ?>

    <?= $form->field($model, 'DocNum')->textInput() ?>

    <?= $form->field($model, 'ReverseEntry')->checkbox() ?>

    <?= $form->field($model, 'ReverseDate')->textInput() ?>

    <?= $form->field($model, 'ReverseJrnl')->textInput() ?>

    <?= $form->field($model, 'CashAdjType')->textInput(['maxlength' => 1]) ?>

    <?= $form->field($model, 'DepositDate')->textInput() ?>

    <?= $form->field($model, 'ReceiptNbr')->textInput() ?>

    <?= $form->field($model, 'AdjType')->textInput(['maxlength' => 1]) ?>

    <?= $form->field($model, 'Invoice')->textInput() ?>

    <?= $form->field($model, 'ClientId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
