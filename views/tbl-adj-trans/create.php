<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblAdjTrans */

$this->title = 'Create Tbl Adj Trans';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Adj Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-adj-trans-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
