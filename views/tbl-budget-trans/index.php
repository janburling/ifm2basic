<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblBudgetTransSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Budget Trans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-budget-trans-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tbl Budget Trans', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'JournalNumber',
            'ResolutionNumber',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
