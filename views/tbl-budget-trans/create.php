<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblBudgetTrans */

$this->title = 'Create Tbl Budget Trans';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Budget Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-budget-trans-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
