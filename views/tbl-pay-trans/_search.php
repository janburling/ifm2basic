<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblPayTransSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-pay-trans-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'JournalNumber') ?>

    <?= $form->field($model, 'VendorId') ?>

    <?= $form->field($model, 'ClientId') ?>

    <?= $form->field($model, 'CashAccount') ?>

    <?= $form->field($model, 'PayAccount') ?>

    <?php // echo $form->field($model, 'VoucherNo') ?>

    <?php // echo $form->field($model, 'Type1099') ?>

    <?php // echo $form->field($model, 'Status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
