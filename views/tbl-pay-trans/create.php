<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblPayTrans */

$this->title = 'Create Tbl Pay Trans';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Pay Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-pay-trans-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
