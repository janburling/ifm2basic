<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblPayTransSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Pay Trans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-pay-trans-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tbl Pay Trans', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'JournalNumber',
            'VendorId',
            'ClientId',
            'CashAccount',
            'PayAccount',
            // 'VoucherNo',
            // 'Type1099',
            // 'Status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
