<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblPayTrans */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-pay-trans-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'JournalNumber')->textInput() ?>

    <?= $form->field($model, 'VendorId')->textInput() ?>

    <?= $form->field($model, 'ClientId')->textInput() ?>

    <?= $form->field($model, 'CashAccount')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'PayAccount')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'VoucherNo')->textInput() ?>

    <?= $form->field($model, 'Type1099')->textInput() ?>

    <?= $form->field($model, 'Status')->textInput(['maxlength' => 1]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
