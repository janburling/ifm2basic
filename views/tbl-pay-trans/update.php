<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblPayTrans */

$this->title = 'Update Tbl Pay Trans: ' . ' ' . $model->JournalNumber;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Pay Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->JournalNumber, 'url' => ['view', 'id' => $model->JournalNumber]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-pay-trans-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
