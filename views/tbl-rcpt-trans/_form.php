<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblRcptTrans */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-rcpt-trans-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'JournalNumber')->textInput() ?>

    <?= $form->field($model, 'ReceiptNumber')->textInput() ?>

    <?= $form->field($model, 'CashAccount')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'DepositDate')->textInput() ?>

    <?= $form->field($model, 'DepositSeq')->textInput() ?>

    <?= $form->field($model, 'CashType')->textInput(['maxlength' => 1]) ?>

    <?= $form->field($model, 'ClientId')->textInput() ?>

    <?= $form->field($model, 'Source')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'Invoice')->textInput() ?>

    <?= $form->field($model, 'CheckNo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
