<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblRcptTransSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-rcpt-trans-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'JournalNumber') ?>

    <?= $form->field($model, 'ReceiptNumber') ?>

    <?= $form->field($model, 'CashAccount') ?>

    <?= $form->field($model, 'DepositDate') ?>

    <?= $form->field($model, 'DepositSeq') ?>

    <?php // echo $form->field($model, 'CashType') ?>

    <?php // echo $form->field($model, 'ClientId') ?>

    <?php // echo $form->field($model, 'Source') ?>

    <?php // echo $form->field($model, 'Invoice') ?>

    <?php // echo $form->field($model, 'CheckNo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
