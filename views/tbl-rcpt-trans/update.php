<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblRcptTrans */

$this->title = 'Update Tbl Rcpt Trans: ' . ' ' . $model->JournalNumber;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Rcpt Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->JournalNumber, 'url' => ['view', 'id' => $model->JournalNumber]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-rcpt-trans-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
