<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblRcptTrans */

$this->title = 'Create Tbl Rcpt Trans';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Rcpt Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-rcpt-trans-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
