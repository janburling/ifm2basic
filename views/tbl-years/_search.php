<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblYearsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-years-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Year') ?>

    <?= $form->field($model, 'Status') ?>

    <?= $form->field($model, 'FiscalStart') ?>

    <?= $form->field($model, 'FiscalEnd') ?>

    <?= $form->field($model, 'CreateDate') ?>

    <?php // echo $form->field($model, 'ModifiedDate') ?>

    <?php // echo $form->field($model, 'Who') ?>

    <?php // echo $form->field($model, 'organizationId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
