<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblYears */

$this->title = 'Update Tbl Years: ' . ' ' . $model->Year;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Year, 'url' => ['view', 'id' => $model->Year]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-years-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
