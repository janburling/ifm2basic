<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblYears */

$this->title = 'Create Tbl Years';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-years-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
