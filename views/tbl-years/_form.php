<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblYears */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-years-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Year')->textInput() ?>

    <?= $form->field($model, 'Status')->textInput(['maxlength' => 1]) ?>

    <?= $form->field($model, 'FiscalStart')->textInput() ?>

    <?= $form->field($model, 'FiscalEnd')->textInput() ?>

    <?= $form->field($model, 'CreateDate')->textInput() ?>

    <?= $form->field($model, 'ModifiedDate')->textInput() ?>

    <?= $form->field($model, 'Who')->textInput() ?>

    <?= $form->field($model, 'organizationId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
