<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblDisbTransSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Disb Trans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-disb-trans-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tbl Disb Trans', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'JournalNumber',
            'VendorId',
            'Payable',
            'CashAccount',
            'VoucherNo',
            // 'Type1099',
            // 'CheckRunNo',
            // 'CheckNo',
            // 'CheckType',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
