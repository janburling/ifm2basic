<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblDisbTrans */

$this->title = 'Create Tbl Disb Trans';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Disb Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-disb-trans-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
