<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblDisbTransSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-disb-trans-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'JournalNumber') ?>

    <?= $form->field($model, 'VendorId') ?>

    <?= $form->field($model, 'Payable') ?>

    <?= $form->field($model, 'CashAccount') ?>

    <?= $form->field($model, 'VoucherNo') ?>

    <?php // echo $form->field($model, 'Type1099') ?>

    <?php // echo $form->field($model, 'CheckRunNo') ?>

    <?php // echo $form->field($model, 'CheckNo') ?>

    <?php // echo $form->field($model, 'CheckType') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
