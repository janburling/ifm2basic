<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblTrans */

$this->title = 'Create Tbl Trans';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-trans-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
