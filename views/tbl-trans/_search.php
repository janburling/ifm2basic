<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblTransSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-trans-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'JournalNumber') ?>

    <?= $form->field($model, 'SequenceNumber') ?>

    <?= $form->field($model, 'Year') ?>

    <?= $form->field($model, 'AccountKey') ?>

    <?= $form->field($model, 'AcctType') ?>

    <?php // echo $form->field($model, 'Source') ?>

    <?php // echo $form->field($model, 'Debit') ?>

    <?php // echo $form->field($model, 'Credit') ?>

    <?php // echo $form->field($model, 'DBCR') ?>

    <?php // echo $form->field($model, 'Balance') ?>

    <?php // echo $form->field($model, 'Reimbursable')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
