<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TblTrans */

$this->title = $model->JournalNumber;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-trans-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'JournalNumber' => $model->JournalNumber, 'SequenceNumber' => $model->SequenceNumber, 'Year' => $model->Year, 'AccountKey' => $model->AccountKey, 'AcctType' => $model->AcctType], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'JournalNumber' => $model->JournalNumber, 'SequenceNumber' => $model->SequenceNumber, 'Year' => $model->Year, 'AccountKey' => $model->AccountKey, 'AcctType' => $model->AcctType], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'JournalNumber',
            'SequenceNumber',
            'Year',
            'AccountKey',
            'AcctType',
            'Source',
            'Debit',
            'Credit',
            'DBCR',
            'Balance',
            'Reimbursable:boolean',
        ],
    ]) ?>

</div>
