<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblTrans */

$this->title = 'Update Tbl Trans: ' . ' ' . $model->JournalNumber;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Trans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->JournalNumber, 'url' => ['view', 'JournalNumber' => $model->JournalNumber, 'SequenceNumber' => $model->SequenceNumber, 'Year' => $model->Year, 'AccountKey' => $model->AccountKey, 'AcctType' => $model->AcctType]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-trans-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
