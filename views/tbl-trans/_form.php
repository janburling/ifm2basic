<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblTrans */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-trans-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'JournalNumber')->textInput() ?>

    <?= $form->field($model, 'SequenceNumber')->textInput() ?>

    <?= $form->field($model, 'Year')->textInput() ?>

    <?= $form->field($model, 'AccountKey')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'AcctType')->textInput(['maxlength' => 1]) ?>

    <?= $form->field($model, 'Source')->textInput(['maxlength' => 1]) ?>

    <?= $form->field($model, 'Debit')->textInput() ?>

    <?= $form->field($model, 'Credit')->textInput() ?>

    <?= $form->field($model, 'DBCR')->textInput(['maxlength' => 1]) ?>

    <?= $form->field($model, 'Balance')->textInput() ?>

    <?= $form->field($model, 'Reimbursable')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
