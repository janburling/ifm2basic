<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblJournal */

$this->title = 'Update Tbl Journal: ' . ' ' . $model->JournalNumber;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Journals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->JournalNumber, 'url' => ['view', 'id' => $model->JournalNumber]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-journal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
