<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblJournal */

$this->title = 'Create Tbl Journal';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Journals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-journal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
