<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TblJournal */

$this->title = $model->JournalNumber;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Journals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-journal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->JournalNumber], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->JournalNumber], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'JournalNumber',
            'TransactionDate',
            'Description',
            'TransType',
            'EntrySource',
            'PJournalNum',
            'SourceJrn',
            'Amount',
            'PriorYear',
            'Status',
            'Who',
            'Createdate',
            'Postdate',
            'WhoApproved',
            'ApproveDate',
            'ModifiedDate',
            'organizationId',
        ],
    ]) ?>

</div>
