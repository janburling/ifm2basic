<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblJournal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-journal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'JournalNumber')->textInput() ?>

    <?= $form->field($model, 'TransactionDate')->textInput() ?>

    <?= $form->field($model, 'Description')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'TransType')->textInput(['maxlength' => 1]) ?>

    <?= $form->field($model, 'EntrySource')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'PJournalNum')->textInput() ?>

    <?= $form->field($model, 'SourceJrn')->textInput() ?>

    <?= $form->field($model, 'Amount')->textInput() ?>

    <?= $form->field($model, 'PriorYear')->textInput(['maxlength' => 1]) ?>

    <?= $form->field($model, 'Status')->textInput(['maxlength' => 1]) ?>

    <?= $form->field($model, 'Who')->textInput() ?>

    <?= $form->field($model, 'Createdate')->textInput() ?>

    <?= $form->field($model, 'Postdate')->textInput() ?>

    <?= $form->field($model, 'WhoApproved')->textInput() ?>

    <?= $form->field($model, 'ApproveDate')->textInput() ?>

    <?= $form->field($model, 'ModifiedDate')->textInput() ?>

    <?= $form->field($model, 'organizationId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
