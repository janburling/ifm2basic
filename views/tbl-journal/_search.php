<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblJournalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-journal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'JournalNumber') ?>

    <?= $form->field($model, 'TransactionDate') ?>

    <?= $form->field($model, 'Description') ?>

    <?= $form->field($model, 'TransType') ?>

    <?= $form->field($model, 'EntrySource') ?>

    <?php // echo $form->field($model, 'PJournalNum') ?>

    <?php // echo $form->field($model, 'SourceJrn') ?>

    <?php // echo $form->field($model, 'Amount') ?>

    <?php // echo $form->field($model, 'PriorYear') ?>

    <?php // echo $form->field($model, 'Status') ?>

    <?php // echo $form->field($model, 'Who') ?>

    <?php // echo $form->field($model, 'Createdate') ?>

    <?php // echo $form->field($model, 'Postdate') ?>

    <?php // echo $form->field($model, 'WhoApproved') ?>

    <?php // echo $form->field($model, 'ApproveDate') ?>

    <?php // echo $form->field($model, 'ModifiedDate') ?>

    <?php // echo $form->field($model, 'organizationId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
