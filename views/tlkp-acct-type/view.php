<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TlkpAcctType */

$this->title = $model->AcctType;
$this->params['breadcrumbs'][] = ['label' => 'Tlkp Acct Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tlkp-acct-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->AcctType], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->AcctType], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'AcctType',
            'Description',
            'PrintSeq',
        ],
    ]) ?>

</div>
