<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblARTrans */

$this->title = 'Create Tbl Artrans';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Artrans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-artrans-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
