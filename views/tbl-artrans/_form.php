<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblARTrans */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-artrans-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'JournalNumber')->textInput() ?>

    <?= $form->field($model, 'Invoice')->textInput() ?>

    <?= $form->field($model, 'Client')->textInput() ?>

    <?= $form->field($model, 'ReceivableAcct')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'ReceivableAcctType')->textInput(['maxlength' => 1]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
