<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblARTransSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-artrans-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'JournalNumber') ?>

    <?= $form->field($model, 'Invoice') ?>

    <?= $form->field($model, 'Client') ?>

    <?= $form->field($model, 'ReceivableAcct') ?>

    <?= $form->field($model, 'ReceivableAcctType') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
