<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblAdjTrans;

/**
 * TblAdjTransSearch represents the model behind the search form about `app\models\TblAdjTrans`.
 */
class TblAdjTransSearch extends TblAdjTrans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'DocYear', 'DocNum', 'ReverseJrnl', 'ReceiptNbr', 'Invoice', 'ClientId'], 'integer'],
            [['ReverseEntry'], 'boolean'],
            [['ReverseDate', 'CashAdjType', 'DepositDate', 'AdjType'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblAdjTrans::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'JournalNumber' => $this->JournalNumber,
            'DocYear' => $this->DocYear,
            'DocNum' => $this->DocNum,
            'ReverseEntry' => $this->ReverseEntry,
            'ReverseDate' => $this->ReverseDate,
            'ReverseJrnl' => $this->ReverseJrnl,
            'DepositDate' => $this->DepositDate,
            'ReceiptNbr' => $this->ReceiptNbr,
            'Invoice' => $this->Invoice,
            'ClientId' => $this->ClientId,
        ]);

        $query->andFilterWhere(['like', 'CashAdjType', $this->CashAdjType])
            ->andFilterWhere(['like', 'AdjType', $this->AdjType]);

        return $dataProvider;
    }
}
