<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblRcptTrans;

/**
 * TblRcptTransSearch represents the model behind the search form about `app\models\TblRcptTrans`.
 */
class TblRcptTransSearch extends TblRcptTrans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'ReceiptNumber', 'DepositSeq', 'ClientId', 'Invoice', 'CheckNo'], 'integer'],
            [['CashAccount', 'DepositDate', 'CashType', 'Source'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblRcptTrans::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'JournalNumber' => $this->JournalNumber,
            'ReceiptNumber' => $this->ReceiptNumber,
            'DepositDate' => $this->DepositDate,
            'DepositSeq' => $this->DepositSeq,
            'ClientId' => $this->ClientId,
            'Invoice' => $this->Invoice,
            'CheckNo' => $this->CheckNo,
        ]);

        $query->andFilterWhere(['like', 'CashAccount', $this->CashAccount])
            ->andFilterWhere(['like', 'CashType', $this->CashType])
            ->andFilterWhere(['like', 'Source', $this->Source]);

        return $dataProvider;
    }
}
