<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblYears;

/**
 * TblYearsSearch represents the model behind the search form about `app\models\TblYears`.
 */
class TblYearsSearch extends TblYears
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Year', 'Who', 'organizationId'], 'integer'],
            [['Status', 'FiscalStart', 'FiscalEnd', 'CreateDate', 'ModifiedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblYears::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Year' => $this->Year,
            'FiscalStart' => $this->FiscalStart,
            'FiscalEnd' => $this->FiscalEnd,
            'CreateDate' => $this->CreateDate,
            'ModifiedDate' => $this->ModifiedDate,
            'Who' => $this->Who,
            'organizationId' => $this->organizationId,
        ]);

        $query->andFilterWhere(['like', 'Status', $this->Status]);

        return $dataProvider;
    }
}
