<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblJournal".
 *
 * @property integer $JournalNumber
 * @property string $TransactionDate
 * @property string $Description
 * @property string $TransType
 * @property string $EntrySource
 * @property integer $PJournalNum
 * @property integer $SourceJrn
 * @property double $Amount
 * @property string $PriorYear
 * @property string $Status
 * @property integer $Who
 * @property string $Createdate
 * @property string $Postdate
 * @property integer $WhoApproved
 * @property string $ApproveDate
 * @property string $ModifiedDate
 * @property integer $organizationId
 */
class TblJournal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblJournal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'EntrySource'], 'required'],
            [['JournalNumber', 'PJournalNum', 'SourceJrn', 'Who', 'WhoApproved', 'organizationId'], 'integer'],
            [['TransactionDate', 'Createdate', 'Postdate', 'ApproveDate', 'ModifiedDate'], 'safe'],
            [['Amount'], 'number'],
            [['Description'], 'string', 'max' => 255],
            [['TransType', 'PriorYear', 'Status'], 'string', 'max' => 1],
            [['EntrySource'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JournalNumber' => 'Journal Number',
            'TransactionDate' => 'Transaction Date',
            'Description' => 'Description',
            'TransType' => 'Trans Type',
            'EntrySource' => 'Entry Source',
            'PJournalNum' => 'Pjournal Num',
            'SourceJrn' => 'Source Jrn',
            'Amount' => 'Amount',
            'PriorYear' => 'Prior Year',
            'Status' => 'Status',
            'Who' => 'Who',
            'Createdate' => 'Createdate',
            'Postdate' => 'Postdate',
            'WhoApproved' => 'Who Approved',
            'ApproveDate' => 'Approve Date',
            'ModifiedDate' => 'Modified Date',
            'organizationId' => 'Organization ID',
        ];
    }
}
