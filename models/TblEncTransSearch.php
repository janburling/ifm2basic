<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblEncTrans;

/**
 * TblEncTransSearch represents the model behind the search form about `app\models\TblEncTrans`.
 */
class TblEncTransSearch extends TblEncTrans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'EncYear', 'EncPONumber', 'VendorId'], 'integer'],
            [['EncFund', 'EStatus'], 'safe'],
            [['Amount', 'Balance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblEncTrans::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'JournalNumber' => $this->JournalNumber,
            'EncYear' => $this->EncYear,
            'EncPONumber' => $this->EncPONumber,
            'VendorId' => $this->VendorId,
            'Amount' => $this->Amount,
            'Balance' => $this->Balance,
        ]);

        $query->andFilterWhere(['like', 'EncFund', $this->EncFund])
            ->andFilterWhere(['like', 'EStatus', $this->EStatus]);

        return $dataProvider;
    }
}
