<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblYears".
 *
 * @property integer $Year
 * @property string $Status
 * @property string $FiscalStart
 * @property string $FiscalEnd
 * @property string $CreateDate
 * @property string $ModifiedDate
 * @property integer $Who
 * @property integer $organizationId
 */
class TblYears extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblYears';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Year'], 'required'],
            [['Year', 'Who', 'organizationId'], 'integer'],
            [['FiscalStart', 'FiscalEnd', 'CreateDate', 'ModifiedDate'], 'safe'],
            [['Status'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Year' => 'Year',
            'Status' => 'Status',
            'FiscalStart' => 'Fiscal Start',
            'FiscalEnd' => 'Fiscal End',
            'CreateDate' => 'Create Date',
            'ModifiedDate' => 'Modified Date',
            'Who' => 'Who',
            'organizationId' => 'Organization ID',
        ];
    }
}
