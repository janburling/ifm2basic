<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblRcptTrans".
 *
 * @property integer $JournalNumber
 * @property integer $ReceiptNumber
 * @property string $CashAccount
 * @property string $DepositDate
 * @property integer $DepositSeq
 * @property string $CashType
 * @property integer $ClientId
 * @property string $Source
 * @property integer $Invoice
 * @property integer $CheckNo
 */
class TblRcptTrans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblRcptTrans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'CashType', 'ClientId'], 'required'],
            [['JournalNumber', 'ReceiptNumber', 'DepositSeq', 'ClientId', 'Invoice', 'CheckNo'], 'integer'],
            [['DepositDate'], 'safe'],
            [['CashAccount', 'Source'], 'string', 'max' => 30],
            [['CashType'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JournalNumber' => 'Journal Number',
            'ReceiptNumber' => 'Receipt Number',
            'CashAccount' => 'Cash Account',
            'DepositDate' => 'Deposit Date',
            'DepositSeq' => 'Deposit Seq',
            'CashType' => 'Cash Type',
            'ClientId' => 'Client ID',
            'Source' => 'Source',
            'Invoice' => 'Invoice',
            'CheckNo' => 'Check No',
        ];
    }
}
