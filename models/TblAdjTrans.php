<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblAdjTrans".
 *
 * @property integer $JournalNumber
 * @property integer $DocYear
 * @property integer $DocNum
 * @property boolean $ReverseEntry
 * @property string $ReverseDate
 * @property integer $ReverseJrnl
 * @property string $CashAdjType
 * @property string $DepositDate
 * @property integer $ReceiptNbr
 * @property string $AdjType
 * @property integer $Invoice
 * @property integer $ClientId
 */
class TblAdjTrans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblAdjTrans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'DocNum', 'ReverseEntry'], 'required'],
            [['JournalNumber', 'DocYear', 'DocNum', 'ReverseJrnl', 'ReceiptNbr', 'Invoice', 'ClientId'], 'integer'],
            [['ReverseEntry'], 'boolean'],
            [['ReverseDate', 'DepositDate'], 'safe'],
            [['CashAdjType', 'AdjType'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JournalNumber' => 'Journal Number',
            'DocYear' => 'Doc Year',
            'DocNum' => 'Doc Num',
            'ReverseEntry' => 'Reverse Entry',
            'ReverseDate' => 'Reverse Date',
            'ReverseJrnl' => 'Reverse Jrnl',
            'CashAdjType' => 'Cash Adj Type',
            'DepositDate' => 'Deposit Date',
            'ReceiptNbr' => 'Receipt Nbr',
            'AdjType' => 'Adj Type',
            'Invoice' => 'Invoice',
            'ClientId' => 'Client ID',
        ];
    }
}
