<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblJournal;

/**
 * TblJournalSearch represents the model behind the search form about `app\models\TblJournal`.
 */
class TblJournalSearch extends TblJournal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'PJournalNum', 'SourceJrn', 'Who', 'WhoApproved', 'organizationId'], 'integer'],
            [['TransactionDate', 'Description', 'TransType', 'EntrySource', 'PriorYear', 'Status', 'Createdate', 'Postdate', 'ApproveDate', 'ModifiedDate'], 'safe'],
            [['Amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblJournal::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'JournalNumber' => $this->JournalNumber,
            'TransactionDate' => $this->TransactionDate,
            'PJournalNum' => $this->PJournalNum,
            'SourceJrn' => $this->SourceJrn,
            'Amount' => $this->Amount,
            'Who' => $this->Who,
            'Createdate' => $this->Createdate,
            'Postdate' => $this->Postdate,
            'WhoApproved' => $this->WhoApproved,
            'ApproveDate' => $this->ApproveDate,
            'ModifiedDate' => $this->ModifiedDate,
            'organizationId' => $this->organizationId,
        ]);

        $query->andFilterWhere(['like', 'Description', $this->Description])
            ->andFilterWhere(['like', 'TransType', $this->TransType])
            ->andFilterWhere(['like', 'EntrySource', $this->EntrySource])
            ->andFilterWhere(['like', 'PriorYear', $this->PriorYear])
            ->andFilterWhere(['like', 'Status', $this->Status]);

        return $dataProvider;
    }
}
