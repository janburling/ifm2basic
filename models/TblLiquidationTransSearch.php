<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblLiquidationTrans;

/**
 * TblLiquidationTransSearch represents the model behind the search form about `app\models\TblLiquidationTrans`.
 */
class TblLiquidationTransSearch extends TblLiquidationTrans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'EncNum', 'Payable'], 'integer'],
            [['Status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblLiquidationTrans::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'JournalNumber' => $this->JournalNumber,
            'EncNum' => $this->EncNum,
            'Payable' => $this->Payable,
        ]);

        $query->andFilterWhere(['like', 'Status', $this->Status]);

        return $dataProvider;
    }
}
