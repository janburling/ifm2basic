<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblBudgetTrans;

/**
 * TblBudgetTransSearch represents the model behind the search form about `app\models\TblBudgetTrans`.
 */
class TblBudgetTransSearch extends TblBudgetTrans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'ResolutionNumber'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblBudgetTrans::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'JournalNumber' => $this->JournalNumber,
            'ResolutionNumber' => $this->ResolutionNumber,
        ]);

        return $dataProvider;
    }
}
