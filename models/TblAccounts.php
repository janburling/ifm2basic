<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblAccounts".
 *
 * @property integer $RecKey
 * @property integer $Year
 * @property string $AcctType
 * @property string $AccountKey
 * @property string $FundCode
 * @property integer $AcctId
 * @property integer $FunctionalUnit
 * @property integer $LocAct
 * @property string $Object
 * @property string $LedgerGroup
 * @property string $AcctClass
 * @property integer $Owner
 * @property integer $BudgetOwner
 * @property integer $SubProgram
 * @property double $Budgeted
 * @property double $Actual
 * @property double $Encumbered
 * @property string $ParentKey
 * @property string $GLParent
 * @property string $BudgetAccount
 * @property string $OldAccountKey
 * @property string $Description
 * @property double $ReimbursablePct
 * @property boolean $PostingLevel
 * @property boolean $BudgetLevel
 * @property double $AdoptedBudget
 * @property string $Status
 * @property string $SummaryGroup
 * @property string $GASBUserField1
 * @property string $GASBUserField2
 * @property string $GASBUserField3
 * @property string $GASBUserField4
 * @property string $GASBUserField5
 * @property string $GASBUserField6
 * @property string $GASBUserField7
 * @property string $GASBUserField8
 * @property string $GASBUserField9
 * @property string $GASBUserField10
 * @property string $PostedDate
 * @property string $Modifieddate
 * @property string $CreatedDate
 * @property integer $Who
 * @property integer $organizationId
 */
class TblAccounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblAccounts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Year', 'AcctType', 'AccountKey', 'FundCode', 'AcctId', 'Owner', 'ParentKey', 'SummaryGroup'], 'required'],
            [['Year', 'AcctId', 'FunctionalUnit', 'LocAct', 'Owner', 'BudgetOwner', 'SubProgram', 'Who', 'organizationId'], 'integer'],
            [['Budgeted', 'Actual', 'Encumbered', 'ReimbursablePct', 'AdoptedBudget'], 'number'],
            [['PostingLevel', 'BudgetLevel'], 'boolean'],
            [['PostedDate', 'Modifieddate', 'CreatedDate'], 'safe'],
            [['AcctType', 'LedgerGroup', 'AcctClass', 'Status', 'SummaryGroup'], 'string', 'max' => 1],
            [['AccountKey', 'ParentKey', 'GLParent', 'BudgetAccount', 'OldAccountKey'], 'string', 'max' => 30],
            [['FundCode'], 'string', 'max' => 3],
            [['Object'], 'string', 'max' => 15],
            [['Description'], 'string', 'max' => 50],
            [['GASBUserField1', 'GASBUserField2', 'GASBUserField3', 'GASBUserField4', 'GASBUserField5', 'GASBUserField6', 'GASBUserField7', 'GASBUserField8', 'GASBUserField9', 'GASBUserField10'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'RecKey' => 'Rec Key',
            'Year' => 'Year',
            'AcctType' => 'Acct Type',
            'AccountKey' => 'Account Key',
            'FundCode' => 'Fund Code',
            'AcctId' => 'Acct ID',
            'FunctionalUnit' => 'Functional Unit',
            'LocAct' => 'Loc Act',
            'Object' => 'Object',
            'LedgerGroup' => 'Ledger Group',
            'AcctClass' => 'Acct Class',
            'Owner' => 'Owner',
            'BudgetOwner' => 'Budget Owner',
            'SubProgram' => 'Sub Program',
            'Budgeted' => 'Budgeted',
            'Actual' => 'Actual',
            'Encumbered' => 'Encumbered',
            'ParentKey' => 'Parent Key',
            'GLParent' => 'Glparent',
            'BudgetAccount' => 'Budget Account',
            'OldAccountKey' => 'Old Account Key',
            'Description' => 'Description',
            'ReimbursablePct' => 'Reimbursable Pct',
            'PostingLevel' => 'Posting Level',
            'BudgetLevel' => 'Budget Level',
            'AdoptedBudget' => 'Adopted Budget',
            'Status' => 'Status',
            'SummaryGroup' => 'Summary Group',
            'GASBUserField1' => 'Gasbuser Field1',
            'GASBUserField2' => 'Gasbuser Field2',
            'GASBUserField3' => 'Gasbuser Field3',
            'GASBUserField4' => 'Gasbuser Field4',
            'GASBUserField5' => 'Gasbuser Field5',
            'GASBUserField6' => 'Gasbuser Field6',
            'GASBUserField7' => 'Gasbuser Field7',
            'GASBUserField8' => 'Gasbuser Field8',
            'GASBUserField9' => 'Gasbuser Field9',
            'GASBUserField10' => 'Gasbuser Field10',
            'PostedDate' => 'Posted Date',
            'Modifieddate' => 'Modifieddate',
            'CreatedDate' => 'Created Date',
            'Who' => 'Who',
            'organizationId' => 'Organization ID',
        ];
    }
}
