<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblLiquidationTrans".
 *
 * @property integer $JournalNumber
 * @property integer $EncNum
 * @property integer $Payable
 * @property string $Status
 */
class TblLiquidationTrans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblLiquidationTrans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'EncNum'], 'required'],
            [['JournalNumber', 'EncNum', 'Payable'], 'integer'],
            [['Status'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JournalNumber' => 'Journal Number',
            'EncNum' => 'Enc Num',
            'Payable' => 'Payable',
            'Status' => 'Status',
        ];
    }
}
