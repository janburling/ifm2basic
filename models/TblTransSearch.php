<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblTrans;

/**
 * TblTransSearch represents the model behind the search form about `app\models\TblTrans`.
 */
class TblTransSearch extends TblTrans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'SequenceNumber', 'Year'], 'integer'],
            [['AccountKey', 'AcctType', 'Source', 'DBCR'], 'safe'],
            [['Debit', 'Credit', 'Balance'], 'number'],
            [['Reimbursable'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblTrans::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'JournalNumber' => $this->JournalNumber,
            'SequenceNumber' => $this->SequenceNumber,
            'Year' => $this->Year,
            'Debit' => $this->Debit,
            'Credit' => $this->Credit,
            'Balance' => $this->Balance,
            'Reimbursable' => $this->Reimbursable,
        ]);

        $query->andFilterWhere(['like', 'AccountKey', $this->AccountKey])
            ->andFilterWhere(['like', 'AcctType', $this->AcctType])
            ->andFilterWhere(['like', 'Source', $this->Source])
            ->andFilterWhere(['like', 'DBCR', $this->DBCR]);

        return $dataProvider;
    }
}
