<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblDisbTrans;

/**
 * TblDisbTransSearch represents the model behind the search form about `app\models\TblDisbTrans`.
 */
class TblDisbTransSearch extends TblDisbTrans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'VendorId', 'Payable', 'VoucherNo', 'Type1099', 'CheckRunNo', 'CheckNo'], 'integer'],
            [['CashAccount', 'CheckType'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblDisbTrans::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'JournalNumber' => $this->JournalNumber,
            'VendorId' => $this->VendorId,
            'Payable' => $this->Payable,
            'VoucherNo' => $this->VoucherNo,
            'Type1099' => $this->Type1099,
            'CheckRunNo' => $this->CheckRunNo,
            'CheckNo' => $this->CheckNo,
        ]);

        $query->andFilterWhere(['like', 'CashAccount', $this->CashAccount])
            ->andFilterWhere(['like', 'CheckType', $this->CheckType]);

        return $dataProvider;
    }
}
