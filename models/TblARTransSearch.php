<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblARTrans;

/**
 * TblARTransSearch represents the model behind the search form about `app\models\TblARTrans`.
 */
class TblARTransSearch extends TblARTrans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'Invoice', 'Client'], 'integer'],
            [['ReceivableAcct', 'ReceivableAcctType'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblARTrans::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'JournalNumber' => $this->JournalNumber,
            'Invoice' => $this->Invoice,
            'Client' => $this->Client,
        ]);

        $query->andFilterWhere(['like', 'ReceivableAcct', $this->ReceivableAcct])
            ->andFilterWhere(['like', 'ReceivableAcctType', $this->ReceivableAcctType]);

        return $dataProvider;
    }
}
