<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tlkpAcctType".
 *
 * @property string $AcctType
 * @property string $Description
 * @property integer $PrintSeq
 */
class TlkpAcctType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tlkpAcctType';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AcctType'], 'required'],
            [['PrintSeq'], 'integer'],
            [['AcctType'], 'string', 'max' => 1],
            [['Description'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'AcctType' => 'Acct Type',
            'Description' => 'Description',
            'PrintSeq' => 'Print Seq',
        ];
    }
}
