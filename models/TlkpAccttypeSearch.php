<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TlkpAcctType;

/**
 * TlkpAccttypeSearch represents the model behind the search form about `app\models\TlkpAcctType`.
 */
class TlkpAccttypeSearch extends TlkpAcctType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AcctType', 'Description'], 'safe'],
            [['PrintSeq'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TlkpAcctType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'PrintSeq' => $this->PrintSeq,
        ]);

        $query->andFilterWhere(['like', 'AcctType', $this->AcctType])
            ->andFilterWhere(['like', 'Description', $this->Description]);

        return $dataProvider;
    }
}
