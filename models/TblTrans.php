<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblTrans".
 *
 * @property integer $JournalNumber
 * @property integer $SequenceNumber
 * @property integer $Year
 * @property string $AccountKey
 * @property string $AcctType
 * @property string $Source
 * @property double $Debit
 * @property double $Credit
 * @property string $DBCR
 * @property double $Balance
 * @property boolean $Reimbursable
 */
class TblTrans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblTrans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'SequenceNumber', 'Year', 'AccountKey', 'AcctType', 'Reimbursable'], 'required'],
            [['JournalNumber', 'SequenceNumber', 'Year'], 'integer'],
            [['Debit', 'Credit', 'Balance'], 'number'],
            [['Reimbursable'], 'boolean'],
            [['AccountKey'], 'string', 'max' => 30],
            [['AcctType', 'Source', 'DBCR'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JournalNumber' => 'Journal Number',
            'SequenceNumber' => 'Sequence Number',
            'Year' => 'Year',
            'AccountKey' => 'Account Key',
            'AcctType' => 'Acct Type',
            'Source' => 'Source',
            'Debit' => 'Debit',
            'Credit' => 'Credit',
            'DBCR' => 'Dbcr',
            'Balance' => 'Balance',
            'Reimbursable' => 'Reimbursable',
        ];
    }
}
