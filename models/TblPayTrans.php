<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblPayTrans".
 *
 * @property integer $JournalNumber
 * @property integer $VendorId
 * @property integer $ClientId
 * @property string $CashAccount
 * @property string $PayAccount
 * @property integer $VoucherNo
 * @property integer $Type1099
 * @property string $Status
 */
class TblPayTrans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblPayTrans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'VendorId', 'VoucherNo'], 'required'],
            [['JournalNumber', 'VendorId', 'ClientId', 'VoucherNo', 'Type1099'], 'integer'],
            [['CashAccount', 'PayAccount'], 'string', 'max' => 30],
            [['Status'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JournalNumber' => 'Journal Number',
            'VendorId' => 'Vendor ID',
            'ClientId' => 'Client ID',
            'CashAccount' => 'Cash Account',
            'PayAccount' => 'Pay Account',
            'VoucherNo' => 'Voucher No',
            'Type1099' => 'Type1099',
            'Status' => 'Status',
        ];
    }
}
