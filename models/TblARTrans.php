<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblARTrans".
 *
 * @property integer $JournalNumber
 * @property integer $Invoice
 * @property integer $Client
 * @property string $ReceivableAcct
 * @property string $ReceivableAcctType
 */
class TblARTrans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblARTrans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'Invoice', 'Client', 'ReceivableAcct', 'ReceivableAcctType'], 'required'],
            [['JournalNumber', 'Invoice', 'Client'], 'integer'],
            [['ReceivableAcct'], 'string', 'max' => 30],
            [['ReceivableAcctType'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JournalNumber' => 'Journal Number',
            'Invoice' => 'Invoice',
            'Client' => 'Client',
            'ReceivableAcct' => 'Receivable Acct',
            'ReceivableAcctType' => 'Receivable Acct Type',
        ];
    }
}
