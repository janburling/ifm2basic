<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblPayTrans;

/**
 * TblPayTransSearch represents the model behind the search form about `app\models\TblPayTrans`.
 */
class TblPayTransSearch extends TblPayTrans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'VendorId', 'ClientId', 'VoucherNo', 'Type1099'], 'integer'],
            [['CashAccount', 'PayAccount', 'Status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblPayTrans::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'JournalNumber' => $this->JournalNumber,
            'VendorId' => $this->VendorId,
            'ClientId' => $this->ClientId,
            'VoucherNo' => $this->VoucherNo,
            'Type1099' => $this->Type1099,
        ]);

        $query->andFilterWhere(['like', 'CashAccount', $this->CashAccount])
            ->andFilterWhere(['like', 'PayAccount', $this->PayAccount])
            ->andFilterWhere(['like', 'Status', $this->Status]);

        return $dataProvider;
    }
}
