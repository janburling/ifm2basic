<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblDisbTrans".
 *
 * @property integer $JournalNumber
 * @property integer $VendorId
 * @property integer $Payable
 * @property string $CashAccount
 * @property integer $VoucherNo
 * @property integer $Type1099
 * @property integer $CheckRunNo
 * @property integer $CheckNo
 * @property string $CheckType
 */
class TblDisbTrans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblDisbTrans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'VendorId', 'CheckRunNo', 'CheckNo'], 'required'],
            [['JournalNumber', 'VendorId', 'Payable', 'VoucherNo', 'Type1099', 'CheckRunNo', 'CheckNo'], 'integer'],
            [['CashAccount'], 'string', 'max' => 30],
            [['CheckType'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JournalNumber' => 'Journal Number',
            'VendorId' => 'Vendor ID',
            'Payable' => 'Payable',
            'CashAccount' => 'Cash Account',
            'VoucherNo' => 'Voucher No',
            'Type1099' => 'Type1099',
            'CheckRunNo' => 'Check Run No',
            'CheckNo' => 'Check No',
            'CheckType' => 'Check Type',
        ];
    }
}
