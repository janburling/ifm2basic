<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblEncTrans".
 *
 * @property integer $JournalNumber
 * @property string $EncFund
 * @property integer $EncYear
 * @property integer $EncPONumber
 * @property integer $VendorId
 * @property double $Amount
 * @property double $Balance
 * @property string $EStatus
 */
class TblEncTrans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblEncTrans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber', 'VendorId'], 'required'],
            [['JournalNumber', 'EncYear', 'EncPONumber', 'VendorId'], 'integer'],
            [['Amount', 'Balance'], 'number'],
            [['EncFund'], 'string', 'max' => 3],
            [['EStatus'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JournalNumber' => 'Journal Number',
            'EncFund' => 'Enc Fund',
            'EncYear' => 'Enc Year',
            'EncPONumber' => 'Enc Ponumber',
            'VendorId' => 'Vendor ID',
            'Amount' => 'Amount',
            'Balance' => 'Balance',
            'EStatus' => 'Estatus',
        ];
    }
}
