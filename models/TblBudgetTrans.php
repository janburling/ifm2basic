<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblBudgetTrans".
 *
 * @property integer $JournalNumber
 * @property integer $ResolutionNumber
 */
class TblBudgetTrans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblBudgetTrans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JournalNumber'], 'required'],
            [['JournalNumber', 'ResolutionNumber'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JournalNumber' => 'Journal Number',
            'ResolutionNumber' => 'Resolution Number',
        ];
    }
}
